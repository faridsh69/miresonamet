<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const NAME_EN = 'miresonamet';
    const NAME = 'می‌رسونمت';
    const PHONE = '۰۹۲۲۳۲۳۱۹۳۶';
    const MERCHANT_CODE = 'ea12b276-429a-11e7-a249-005056a205be';
    const PAGE_SIZE = 25;
    const SMS_USER = 's.farid.sh69';
    const SMS_PASS = '39026';
    const SMS_PHONE = '50005708616261';
    const PEYK = 2000;
    const OFF = 15;

    public function __construct()
    {
        \Meta::set('title',self::NAME);
        \Meta::set('keywords','فروش,فروشگاه,سفارش میوه,سفارش لباس زیر,ظرف');
    	\Meta::set('description','این سامانه فروشگاه اینترنتی می باشد.');
    }

    public function _calculate_product($orders)
    {
    	foreach($orders as $order)
        {
            $products = [];
            if(json_decode($order->products) != null )  {
                foreach( json_decode($order->products) as $product )
                {
                    $products[] = \App\Models\Product::where('id', $product)->first();
                }
            }
            $order->uniqe_products = collect($products)->groupBy('id');
        }
        return $orders;
    }
}