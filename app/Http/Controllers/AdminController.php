<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use SoapClient;

class AdminController extends Controller
{
    public function getDashboard()
    {
        return view('admin.first-dashboard');
    }

    public function getShop()
    {
        $shop = \App\Models\Shop::first();
        
        return view('admin.shop')->withShop($shop);
    }

    public function getRemoveProduct($id)
    {
        \App\Models\Product::where('id', $id)->delete();

        return redirect()->back();
    }

    public function getEditProduct($id)
    {
        $products = \App\Models\Product::orderBy('id','dsc')->paginate(self::PAGE_SIZE);
        $product = \App\Models\Product::where('id', $id)->first();

        return view('admin.product')->withProduct($product)->withProducts($products);
    }

    public function postProduct(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'type_id' => 'required|exists:types,id',
            'product_image' => 'image|mimes:jpeg,jpg,bmp,png,gif|max:500',
        ])->validate();
        \Log::info('postProduct with data: ' . json_encode($request->all()) . ' by user_id: '. \Auth::id() );
        if( $request['id'] ){
            $product = \App\Models\Product::where('id',$request['id'])->update(
                [
                'name' => $request['name'],
                'type_id' => $request['type_id'],
                'price' => $request['price'],
                'user_id' => $request['user_id'],
                'toharbastebandi' => $request['toharbastebandi'],
                'bastebandi' => $request['bastebandi'],
                'description' => $request['description'],
                'mojodi' => $request['mojodi'],
                'rang' => $request['rang'],
                'jens' => $request['jens'],
                'barcode' => $request['barcode'],
                'sazande' => $request['sazande'],
                'garanti' => $request['garanti'],
                'size' => $request['size'],
                'code' => $request['code'],
                'vazn' => $request['vazn'],
                'year' => $request['year'],
                'applicant' => $request['applicant'],
            ]);
        }else{
            $product = \App\Models\Product::create(
            [
                'name' => $request['name'],
                'type_id' => $request['type_id'],
                'price' => $request['price'],
                'user_id' => $request['user_id'],
                'toharbastebandi' => $request['toharbastebandi'],
                'bastebandi' => $request['bastebandi'],
                'description' => $request['description'],
                'mojodi' => $request['mojodi'],
                'rang' => $request['rang'],
                'jens' => $request['jens'],
                'barcode' => $request['barcode'],
                'sazande' => $request['sazande'],
                'garanti' => $request['garanti'],
                'size' => $request['size'],
                'code' => $request['code'],
                'vazn' => $request['vazn'],
                'year' => $request['year'],
                'applicant' => $request['applicant'],
            ]);
        }

        $data = $request['cropped_image'];
        $file_name = $request['file_name'];
        if($data){
            if(!$file_name){
                $file_name = '00.png';
            }
            $image = [
                'name' => $file_name, 
                'description' => 'product_image', 
                'mime_type' => 'image/png', 
                'size' => 1];
            $image = \App\Models\Image::create($image);
            $destination_path = storage_path() . '/product';
            $data = explode(",", $data)[1];
            $src = $destination_path . '/'. $image->id.'-'.$image->name;
            try {
                file_put_contents($src, base64_decode($data));
            } catch (Exception $e) {
                Log::error('upload file error');
                Log::error($e);
            }
            if( $request['id'] ){
                $product = \App\Models\Product::where('id',$request['id'])->first();
            }
            $product->image_id = $image->id;
            $product->save();
        }

        $request->session()->flash('alert-success', 'اطلاعات محصولات شما با موفقیت ثبت شد.');

        return redirect('/admin/product');
    }

    public function getProduct()
    {
        $products = \App\Models\Product::orderBy('id','dsc')->paginate(self::PAGE_SIZE);

        return view('admin.product')->withProducts($products);
    }

    public function getRemoveType($id)
    {
        \App\Models\Type::where('id', $id)->delete();

        return redirect()->back();
    }

    public function getEditType($id)
    {
        $type_edited = \App\Models\Type::where('id', $id)->first();
        $types = \App\Models\Type::orderBy('id','dsc')->paginate(self::PAGE_SIZE);

        return view('admin.type')->withTypes($types)->withTypeEdited($type_edited);
    }

    public function postType(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required',
        ])->validate();
        \Log::info('type with data: ' . json_encode($request->all()) . ' by user_id: '. \Auth::id() );
        $type = \App\Models\Type::updateOrCreate(
            [
                'id' => $request['id'],
            ],[
                'type_id' => $request['type_id'],
                'name' => $request['name']
            ]);
       
        $request->session()->flash('alert-success', 'اطلاعات دسته بندی با موفقیت ثبت شد.');

        return redirect('/admin/type');
    }

    public function getType()
    {
        $types = \App\Models\Type::orderBy('id','dsc')->paginate(self::PAGE_SIZE);

        return view('admin.type')->withTypes($types);
    }

    public function getComment()
    {
        $comments = \App\Models\Comment::simplePaginate(self::PAGE_SIZE);

        return view('admin.comment')->withComments($comments);
    }

    public function getUserLogin($id)
    {
        if (\Auth::loginUsingId($id)):
            return redirect('/');
        else:
            return back()->withError('Error occurred.');
        endif;
    }
    public function postReportVariz(Request $request)
    {
        \Validator::make($request->all(), [
            'restaurant_id' => 'required|exists:restaurants,id',
            'Invoice_number' => 'required',
            'price' => 'required|numeric', 
        ])->validate();
        $restaurant_pay = \App\Models\RestaurantPayment::create([
            'restaurant_id' => $request['restaurant_id'],
            'price' => $request['price'],
            'Invoice_number' => $request['Invoice_number'],
            'user_id' => \Auth::user()->id ,
            ]);
            \Log::info('پرداخت به رستوران به شماره پرداخت: ' . $restaurant_pay->id . ' by user_id: '. \Auth::id() );
        return redirect()->back();
    }


    public function postNoticeEmail()
    {
        return redirect()->back();
    }

    public function postNoticeSms(Request $request)
    {
         \Validator::make($request->all(), [
            'who' => 'required',
            'sms' => 'required|max:100',
        ])->validate();
        ini_set("soap.wsdl_cache_enabled", "0");
        $sms_client = new \SoapClient(
            'http://payamak-service.ir/SendService.svc?wsdl'
            , array('encoding'=>'UTF-8'));
        try 
        {
            $x = $sms_client->SendSMS([ 
                'userName' => "s.farid.sh69",
                'password' => "39026",
                'fromNumber' => "50005708616261",
                'toNumbers' => ["09106801685"],
                'messageContent' => $request['sms'],
                'isFlash' => false,
                'recId' => [],
                // 'status' => []
                ]);
            \Log::info('sms با متن '.$request['sms'].' فرستاده شد به: فرید با کد: '. $x->SendSMSResult);
        } 
        catch (Exception $e) 
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        return redirect()->back();
    }

    public function getverifyCredit()
    {

        \Meta::set('title','بازگشت از بانک');
        // risky
        $payment = \App\Models\Payment::where('user_id', \Auth::id())->orderBy('id','dsc')->first();
        $MerchantID = self::MERCHANT_CODE;
        $Amount = $payment->amount;
        $Authority = $_GET['Authority'];

        if ($_GET['Status'] == 'OK') {
            $client = new \SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl',array('encoding' => 'UTF-8')); 
            $result = $client->PaymentVerification(
                [
                'MerchantID' => $MerchantID,
                'Authority' => $Authority,
                'Amount'   => $Amount,
                ]
            );
            if ($result->Status == 100) {
                \Log::info('پرداخت با موفقیت انجام شد با شماره : '. $payment->id . ' invN : ' .$result->RefID);
                $code = 1;
                $user = \Auth::user();
                $user->credit += $Amount;
                $user->save();
                $payment->description = $result->RefID;
                $payment->status = 1;
                $payment->Invoice_number = (int)$Authority;
                // vase taraf sms bezan
            } else {
                \Log::warning('پرداخت با خطا مواجه شده با شماره: '. $payment->id . ' status : ' .$result->Status);
                $code = 2;
                $payment->Invoice_number = (int)$Authority;
                $payment->description = self::_getErrorZarrinpal($result->Status);
            }
        } else {
            \Log::warning('کاربر پرداخت به شماره ان را کنسل کرده: '. $payment->id );
            $code = 3;
            $payment->Invoice_number = (int)$Authority;
            $payment->description = 'کاربر کنسل کرده';
        }
        $payment->save();
        return view('verify-charge')->withCode($code);
    }

    public function getChargeCredit()
    {
        return view('admin.charge-credit');
    }
    public function postChargeCredit(Request $request)
    {
        \Validator::make($request->all(), [
            'charge-credit' => 'required|min:1000|numeric',
        ])->validate();
        $price = $request['charge-credit'];

        $payment = \App\Models\Payment::create([
            'user_id' => \Auth::id(),
            'user_ip' => \Requests::ip(),
            'user_agent' => \Requests::header('User-Agent'),
            'amount' => $price,
            'Invoice_date' => 'خرید اعتبار',
            'description' => 'در حال ورود به بانک',
            'status' => 0,
            ]);
            \Log::info('در حال ورود به بانک با خرید اعتبار: ' . $price . ' by user_id: '. \Auth::id() 
            . ' with payment_id: ' . $payment->id );
        $MerchantID = self::MERCHANT_CODE;           
        $Amount = $price; 
        $Description = 'سفارش از ' . self::NAME;  
        $Email = \Auth::user()->email; 
        $Mobile = \Auth::user()->phone; 
        $CallbackURL = url('verifyCredit'); 
          
        // URL also Can be https://ir.zarinpal.com/pg/services/WebGate/wsdl
        $client = new SoapClient('https://de.zarinpal.com/pg/services/WebGate/wsdl', array('encoding' => 'UTF-8')); 
          
        $result = $client->PaymentRequest(
            array(
                'MerchantID'   => $MerchantID,
                'Amount'   => $Amount,
                'Description'   => $Description,
                'Email'   => $Email,
                'Mobile'   => $Mobile,
                'CallbackURL'   => $CallbackURL
            )
        );

        if($result->Status == 100)
        {
            $payment->Invoice_number = $result->Authority;
            $payment->save();
            Header('Location: https://www.zarinpal.com/pg/StartPay/'.$result->Authority);exit;
        } else {
            $payment->description = 'اطلاعات بانک تو سیستم غلطه';
            $payment->save();
            dd('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "اطلاعات بانک اشتباه" رخ داده است. سریعا مشکل رفع می شود',$result->Status);
            \Log::warning('لطفا با شماره ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید و بگویید مشکل "اطلاعات بانک اشتباه" رخ داده است. سریعا مشکل رفع می شود',' کد خطا: '.$result->Status );
        }
    }


    public function getNoticeManage()
    {
        return view('admin.manage.notice');
    }

    public function getPaymentManage()
    {
        $payments = \App\Models\Payment::orderBy('id', 'desc')->simplePaginate(self::PAGE_SIZE);

        return view('admin.payment')->withPayments($payments);
    }

    public function getMyPayment()
    {
        $payments = \App\Models\Payment::where('user_id',\Auth::id())->orderBy('id', 'desc')->simplePaginate(self::PAGE_SIZE);

        return view('admin.payment')->withPayments($payments);
    }

    public function getReport()
    {
        $orders = \App\Models\Order::whereIn('status', ['receive','prepare','send'])->get();

        return view('admin.report')->withOrders($orders);
    }

    public function getMyReport()
    {
        if(\Gate::allows('restaurant_manager')){
            $restaurant = \App\Models\Restaurant::where('user_id',\Auth::id())->first();
            if($restaurant){
                $orders = \App\Models\Order::where('restaurant_id', $restaurant->id)
                ->whereIn('status', ['paid','prepare','paycredit','willpay'])->get();

                return view('admin.report')->withOrders($orders);
            }else{
                return redirect('/admin/restaurant/my-restaurant/0');
            }
        }else{
            dd('شما دچار اشتباه سو استفاده از منشی بودن شدید - هرچه سریعتر با ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید');
            \Log::warning('دچار اشتباه سو استفاده از منشی بودن شدید '.\Auth::id());
        }
    }
    
    public function postOrderManageEdit($id,Request $request)
    {
        $order = \App\Models\Order::where('id',$id)->first();
        if($order->status == 'paid' || $order->status == 'willpay' || $order->status == 'paycredit')
        {
            // $order->foods = json_encode($request['food_ids']);
            $last_price = $order->price;
            $order->price = $request['price'];
            $order->save();

            $user = $order->user;
            $user->credit += ( $last_price - $request['price']);
            $user->save();

            return redirect('/admin/order/manage');
        }
    }

    public function getOrderManageEdit($id)
    {
        $order = \App\Models\Order::where('id',$id)->first();
        if($order->status == 'paid' || $order->status == 'willpay' ||$order->status == 'paycredit')
        {
            $orders = self::_calculate_product(array($order));
            return view('admin.order-edit')->withOrder($order);
        }
    }

    public function getOrderManage(Request $request)
    {
        $orders = \App\Models\Order::orderBy('id', 'desc')
        ->whereNotIn('status', ['select','address','checkout','failed'])->simplePaginate(self::PAGE_SIZE)->appends(['name' => $request->name]);

        $orders = self::_calculate_product($orders);

        return view('admin.order')->withOrders($orders)->withUse('manage');
    }

    public function getSend($id)
    {
        $order = \App\Models\Order::where('id', $id)->first();
        if($order->status == 'preparewillpay')
        {
            $order->status = 'sendwillpay';            
        }
        else
        {
            $order->status = 'send';
        } 
        // $now = \Carbon\Carbon::now();
        // $sendTime = \Carbon\Carbon::parse($order->updated_at);
        // $diff = $sendTime->diffInSeconds($now);
        // $order->prepare_time = $diff;
        $order->save();

        return redirect()->back();
    }

    public function getReceive($id)
    {
        $order = \App\Models\Order::where('id', $id)->first();
        if($order->status == 'sendwillpay')
        {
            $order->status = 'receivewillpay';            
        }
        else
        {
            $order->status = 'receive';
        }
        $order->save();

        return redirect()->back();
    }

    public function getReject($id)
    {
        $order = \App\Models\Order::where('id', $id)->first();
        if($order->status == 'sendwillpay')
        {
            $order->status = 'rejectwillpay';            
        }
        else
        {
            $order->status = 'reject';
        }
        $order->save();

        return redirect()->back();
    }

    
    public function getPrint($id)
    {
        $order = \App\Models\Order::where('id', $id)->first();
        $products = [];
        foreach(json_decode($order->products) as $product_id)
        {
            $products[] = \App\Models\Product::where('id', $product_id)->first();
        }
        $products = collect($products);
        
        return view('admin.print')->withOrder($order)->withProducts($products);
    }

    public function getApprove($id)
    {
        $order = \App\Models\Order::where('id', $id)->first();
        if($order->status == 'willpay')
        {
            $order->status = 'preparewillpay';
        }
        else
        {
            $order->status = 'prepare';
        } 
        $order->save();
        
        return redirect()->back();
    }

    public function getMyOrder()
    {
        $orders = \App\Models\Order::where('status','!=','')->where('user_id', \Auth::id())->orderBy('id', 'desc')->simplePaginate(self::PAGE_SIZE);

        $orders = self::_calculate_product($orders);

        return view('admin.order')->withOrders($orders)->withUse('user');
    }

    
    public function getRemoveMyFood($id)
    {
        $food = \App\Models\Food::where('id',$id)->first();
        $food->ready = 0;
        $food->save();
        \Log::info('غذای با کد ان غیر فعال شده است: ' . $id . ' by user_id: '. \Auth::id() );
        $request = Request();
        $request->session()->flash('alert-warning', 'غذای مورد نظر غیر فعال شد.');
        
        return redirect()->back();
    }

    public function getActiveMyFood($id)
    {
        $food = \App\Models\Food::where('id',$id)->first();
        $food->ready = 1;
        $food->save();
        \Log::info('غذای با کد ان ففعال شده است: ' . $id . ' by user_id: '. \Auth::id() );
        $request = Request();
        $request->session()->flash('alert-warning', 'غذای مورد نظر غیر فعال شد.');
        
        return redirect()->back();
    }

    public function getEditMyFood($id)
    {
        $food = \App\Models\Food::where('id',$id)->first();
        $restaurant = \App\Models\Restaurant::where('id',$food->restaurant->id)->first();
        $foods = \App\Models\Food::where('restaurant_id',$restaurant->id)->orderBy('id','dsc')->get();

        return view('admin.restaurant.food')->withFoods($foods)->withFood($food)->withRestaurant($restaurant);
    }

    public function postUpload(Request $request)
    {
        $data = $request['cropped_image'][0];
        echo('<img src="'. $data .'">');
        $data = explode(",", $data)[1];
        echo('<img src="data:image/jpeg;base64,'. $data .'">');
        $destination_path = storage_path();
        $src = $destination_path.'\uploaded.png';
        file_put_contents($src, base64_decode($data));
    }

	public function postRestaurantMyFood(Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required|numeric',
            'type' => 'required|exists:types,id',
        ])->validate();
        \Log::info('غذای جدد ثبت شد یا ویرایش شد با این اطلاعات: by user_id: '. \Auth::id() );
        $food = \App\Models\Food::updateOrCreate(
            [
                'name' => $request['name'],
                'restaurant_id' => $request['restaurant_id']
            ],[
                'content' => $request['content'],
                'price' => $request['price'],
                'type_id' => $request['type'],
                'ready' => 1,
            ]);
        $data = $request['cropped_image'];
        $file_name = $request['file_name'];
        if($data){
            if(!$file_name){
                $file_name = '00.png';
            }
            $image = [
                'name' => $file_name, 
                'description' => 'food_image', 
                'mime_type' => 'image/png', 
                'size' => 1];
            $image = \App\Models\Image::create($image);
            $destination_path = storage_path() . '/food';
            $data = explode(",", $data)[1];
            $src = $destination_path . '/'. $image->id.'-'.$image->name;
            try {
                file_put_contents($src, base64_decode($data));
            } catch (Exception $e) {
                Log::error('upload file error');
                Log::error($e);
            }
            $food->image_id = $image->id;
            $food->save();
        }

        $request->session()->flash('alert-success', 'اطلاعات منوی غذایی شما با موفقیت ثبت شد.');

        return redirect()->back();
    }

    public function getRestaurantMyFood($name,Request $request)
    {   
        $id = explode(".", $name)[0];
        $restaurant = \App\Models\Restaurant::where('id', $id)->first();
        if($restaurant)
        {
            $foods = \App\Models\Food::where('restaurant_id', $restaurant->id)->orderBy('id', 'desc')->get();
            return view('admin.restaurant.food')->withFoods($foods)->withRestaurant($restaurant);
        }
        return redirect('/');
    }   
    
    public function getRestaurantManage(Request $request)
    {   
        $restaurants = \App\Models\Restaurant::where('name', 'like', '%'.$request->name.'%')->orderBy('id', 'desc')->simplePaginate(self::PAGE_SIZE)->appends(['name' => $request->name]);

        return view('admin.manage.restaurant')->withRestaurants($restaurants);
    }

    public function postRestaurantMy($id,Request $request)
    {
        \Validator::make($request->all(), [
            'name' => 'required',
            'user_id' => 'required',
            'address' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'city_id' => 'required|exists:cities,id',
            'phone' => 'required|numeric',
            'peyk_in' => 'required|numeric',
            'minimum_price' => 'required|numeric',
            'credit_card' => 'required',
            'restaurant_image' => 'image|mimes:jpeg,jpg,bmp,png,gif|max:200',
            // 'type' => 'required',
            // 'off' => 'numeric',
        ])->validate();
        $restaurant = \App\Models\Restaurant::updateOrCreate(['user_id' => $request['user_id']],[
            'name' => $request['name'],
            'address' => $request['address'],
            'start_time' => $request['start_time'],
            'end_time' => $request['end_time'],
            'off' => ($request['off'] ? $request['off'] : 0),
            'city_id' => $request['city_id'],
            'phone' => $request['phone'],
            'peyk_in' => $request['peyk_in'],
            // 'peyk_out' => $request['peyk_out'],
            'minimum_price' => $request['minimum_price'],
            'credit_card' => $request['credit_card'],
        ]);
        if($request['type'])
        {
            $restaurant->types()->sync([], true);
            $restaurant->types()->sync($request['type'], true);
        }

        $file = $request->files->get('restaurant_image');

        if($file){
            $image = [
                'name' => $file->getClientOriginalName(), 
                'description' => 'restaurant_image', 
                'mime_type' => $file->getMimeType(), 
                'size' => $file->getSize()];
            $image = \App\Models\Image::firstOrCreate($image);

            $destination_path = storage_path() . '/restaurant';
            try {
                $file->move($destination_path, $image->id.'-'.$image->name );
            } catch (Exception $e) {
                Log::error('upload file error');
                Log::error($e);
            }
            $restaurant->image_id = $image->id;
            $restaurant->save();
        }

        $request->session()->flash('alert-success', 'اطلاعات رستوران شما با موفقیت ثبت شد.');

        \Log::info('اطلاعات رستوران ویرایش شد با اینا : ' . json_encode($request->all()) . ' by user_id: '. \Auth::id() );

        return redirect()->back();
    }

    public function getRestaurantMy($id)
    {
        if($id == 0) {
            $restaurant = \App\Models\Restaurant::where('user_id',\Auth::id())->first();
        }else{
            $restaurant = \App\Models\Restaurant::where('id',$id)->first();
        }
        if (\Gate::denies('secretary')) {
            if($restaurant && $restaurant->user_id != \Auth::id()) {
                dd('شما دچار اشتباه بزرگی شدید - هرچه سریعتر با ۰۹۱۰۶۸۰۱۶۸۵ تماس بگیرید');
                \Log::warning('دچار اشتباه بزرگی شدید '.\Auth::id()); 
            }
        }
        $restaurant_types = [];
        $cities = \App\Models\City::select('id','name')->get();
        $types = \App\Models\Type::select('id','name')->get();
        if($restaurant){
            foreach($restaurant->types as $type )
            {
                $restaurant_types[] = $type->id;
            } 
        }

        return view('admin.restaurant.detail')->withRestaurant($restaurant)->withCities($cities)->withTypes($types)->withRestaurantTypes($restaurant_types);
    }

	public function getLog()
    {
        $logFile = file(storage_path().'/logs/laravel.log');
        $logFile = collect($logFile)->reverse();
        \Log::info('first log!');
        \Meta::set('title','log');

        return view('admin.manage.log')->withLog($logFile);
    }

    public function getChangePassword()
    {
        return view('admin.change-password');
    }
    public function postChangePassword(Request $request)
    {
        \Validator::make($request->all(), [
            'newpassword' => 'required',
            'oldpassword' => 'required',
        ])->validate();
        \App\User::where('id', \Auth::id() )->update([
            'password' => bcrypt($request['newpassword']),
        ]);

        $request->session()->flash('alert-success', 'رمز عبور شما با موفقیت به '. $request['newpassword'] .' تغییر یافت.');

        return redirect()->back();
    }

    public function postRole(Request $request)
	{
		$this->validate($request, [
			'role_id' => 'required|in:' . implode(",", \App\Models\Role::orderBy('id', 'asc')->pluck('id')->toArray()),
			'user_id' => 'required|in:' . implode(",", \App\User::orderBy('id', 'asc')->pluck('id')->toArray()),
		]);
		\App\User::where('id', $request->get('user_id'))->first()->roles()->sync([$request->get('role_id')], false);
        \Log::info('role_id: '. $request->get('role_id') . ' to user_id: ' . $request->get('user_id') . 
        ' by user_id: ' . \Auth::id() );
		return redirect()->back();
	}

    public function getUserManage(Request $request)
	{
		\Meta::set('title','مدیریت کاربران');

		$users = \App\User::orderBy('id', 'desc')->where('last_name', 'like', '%'.$request->name.'%')
            ->where('phone', 'like', '%'.$request->phone.'%')->simplePaginate(self::PAGE_SIZE)
            ->appends(['name' => $request->name]);
		
		return view('admin.manage.user')->withUsers($users);
	}

    public function getLogout()
    {
        \Auth::logout();
        
        return redirect('/');
    }

    // public function getRemoveAddress($id)
    // {
    //     \App\Models\Address::where('id',$id)->where('user_id', \Auth::id())->delete();
    //     return redirect()->back();
    // }

	public function postProfile(Request $request)
	{
		\Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'phone' => 'required|numeric|digits:11',
            'user_image' => 'image|mimes:jpeg,jpg,bmp,png,gif|max:4000',
        ])->validate();
        \App\User::where('id', \Auth::id() )->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'phone' => $request['phone'],
        ]);
        if($request['address'])
        {
            \App\Models\Address::firstOrCreate([
                'name' => $request['address'],
                'user_id' => \Auth::id()
            ]);
        }
        \Log::info('آدرسی وارد شده با نام : '. $request['address'] . ' +by user_id: ' . \Auth::id());
        $request->session()->flash('alert-success', 'تغییرات شما با موفقیت ذخیره شد.');

		return redirect()->back();
	}

	public function getProfile() 
    {
    	\Meta::set('title','پروفایل');

        return view('admin.profile');
    }
}
