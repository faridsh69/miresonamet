<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function getCity($id)
    {
        $cities = \App\Models\City::where('province_id',$id)->get();
        return $cities;
    }
    public function getType($type)
    {
        if($type == 0){
            $type = 1;
            // inja bayad biaim o tarkibi az mahsola ro besh bedim
        }
        $types = [];
        $types[] = \App\Models\Type::where('id', $type)->first()->id;
        foreach(\App\Models\Type::where('type_id', $type)->get() as $type){
            $types[] = $type->id;
        }
        $products = \App\Models\Product::whereIn('type_id', $types)->orderBy('id','dsc')->get();
        foreach($products as $product)
        {
            if($product->image_id == '')
            {
                $product->image_id = '';
                $product->image_name = 'type-' . $product->type->id . '.png';
            }else{
                $product->image_name = '-' . \App\Models\Image::where('id',$product->image_id)->first()->name;
            }
        }
        $types = \App\Models\Type::whereIn('id',$types)->get();
        
        return ['products' => $products, 'types' => $types ];

    }

    public function postAddress($id,Request $request)
    {
        $order = \App\Models\Order::where('id', $id)->update(
            ['address_id' => $request->input('address'),
            'status' => 'checkout',
            'description' => $request->input('description'),
        ]);        
    }

    public function getAddress($user_id)
    {
        return \App\Models\Address::where('user_id', $user_id )->orderBy('id','dsc')->get();
    }

    public function postOrder($id,Request $request)
    {
        $user_id = $request['user_id'];
        if ($user_id == 0 )
        {
            $order = \App\Models\Order::updateOrCreate(
            [
                'user_ip' => \Requests::ip(),
                'user_id' => null,
                ],[
                'products' => json_encode($request['product_ids']),
                'status' => 'select'
            ]);
        } else {
            $order = \App\Models\Order::where('user_ip', \Requests::ip())->where('user_id', $user_id)->whereIn('status',['select','address','checkout','failed'])->first();
            if($order)
            {
                $order->products = json_encode($request['product_ids']);
                $order->status = 'select';
                $order->save();
            }else{
                $order = \App\Models\Order::create(
                [
                    'user_ip' => \Requests::ip(),
                    'user_id' => $user_id,
                    'products' => json_encode($request['product_ids']),
                    'status' => 'select'
                ]);
            }
        }
        
        return $order->id;
    }

    public function getOrder($user_id)
    {
        if($user_id == "0")
        {
            $order = \App\Models\Order::where('user_ip', \Requests::ip())->where('user_id', null)
                ->whereIn('status',['select','address','checkout','failed'])->first();
        }else{
            $order = \App\Models\Order::where('user_id', $user_id )
            ->whereIn('status',['select','address','checkout','failed'] )->first();
        }

        $products = $product_ids = [];
        if($order)
        {
            foreach(json_decode($order->products) as $product_id)
            {
                $product_item = \App\Models\Product::where('id', $product_id)->first();
                if( $product_item )
                {
                    $products[] = $product_item ;
                    $product_ids[] = $product_id ; 
                }
            }
        }
        // return $order->id;
        return [ 'products' => $products, 'product_ids' => $product_ids ] ;

    }

    public function postPutAddress($id,Request $request)
    {
        \App\Models\Address::firstOrCreate([
            'name' => $request['address'],
            'user_id' => $id
        ]);
        return 'success';
    }
}
