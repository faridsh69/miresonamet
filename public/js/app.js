Vue.component('product-type', {
  	template: `
  	<div>
	<div class="row">
		<div class="seperate"></div>
		<nav class="filter-nav" data-spy="affix" id="filter-nav" data-offset-top="90">
		    <div class="container">
		    	<div class="row">
	    			<div class="col-xs-4">
					    <input type="text" class="form-control" placeholder="محصول یا دسته..." v-model="productName">	 
	    			</div>
	    			<div class="col-xs-3">
	    				<select class="form-control" v-model="selectedType">
							<option v-for="type in types" v-bind:value="type.id">
								{{ type.name }}
							</option>
						</select>	 
	    			</div>
	    			<div class="col-xs-3 col-md-2">
	    				حداکثر قیمت:
	    				{{ productPrice }} هزارتومان
					    <input type="range" placeholder="محصول یا دسته..." v-model="productPrice">	 
	    			</div>
				</div>
		    </div>
		</nav>
		<div class="affix-fixer"></div>
	</div>
	<div class="row text-center">
		<div style="color:#48f;margin-bottom:-25px;" v-if="loading">
			<div class="loading"></div>
			در حال بارگزاری...
		</div>
	</div>
  	<div class="row" v-if="!loading">
		<h3 class="text-center page-header">قفسه ظروف {{ types[0].name }}</h3>
		<div class="col-xs-6 col-sm-8 col-md-9">
			<div class="row">
				<div v-for="product in products" v-if="product.price < 1000 * productPrice && 
				product.name.indexOf(productName) >= 0 &&  ( product.type_id == selectedType || selectedType == '' || selectedType == type )" class="col-lg-3 col-md-4 col-sm-6 text-center product-block">					
					<div v-on:click="addToCart(product)" class="product-card"
					v-bind:class="selectedProductsId.indexOf(product.id)>= 0 ? 'product-selected' : ''">
						<div v-if="selectedProductsId.indexOf(product.id)>= 0" 
						class="label label-info numberOfProduct">
							<span> {{ numberProduct(product.id) | persian_digits }}</span>
							عدد
						</div>
						<div class="">
							<div class="half-seperate"></div>
							<div class="product-image-container">
								<img v-bind:src="'/storage/product/'+product.image_id+product.image_name" 
								class="product-image" v-bind:alt="product.name">
							</div>
							<div class="half-seperate"></div>
							<div class="bold big-size" style="color:#3361ff">
								<span class="">{{ product.name }}</span>
							</div>
							<div class="half-seperate"></div>
							<div style="color: #999">
								<span>{{ product.bastebandi }}</span>
								<span>{{ product.toharbastebandi }} تایی</span>
							</div>
							<div class="seperate"></div>
							<div class="">
								<span style="color:rgba(60,155,10,1);margin-left: 10px" class="bold big-size">
									{{ product.price | persian_digits}}	
									تومان						
								</span>
								<del style="color: #aaa;">
									{{ product.price | persian_digits}}
								</del>
							</div>	
						</div>
					</div>
					<a v-bind:href=" '/product/'+product.id + '.' + product.name " class="product-a">مشاهده جزئیات</a>
				</div>
			</div>
		</div>
	</div>
	<div class="shopping-card">
		<div class="row">
			<div class="col-xs-12">
				<!-- <button class="btn btn-default" style="margin-top:-10px;padding: 4px;padding-bottom: 0px">
					<span class="glyphicon glyphicon-resize-small"></span>
				</button>
				<button class="btn btn-default" style="margin-top:-10px;padding: 4px;padding-bottom: 0px">
					<span class="glyphicon glyphicon-resize-full"></span>
				</button> -->
				<div class="table-responsive">
				<table class="table">
				<thead>
				<tr>
					<th>
						تعداد
						<br>
						(کارتن)	
					</th>
					<th>
						نام محصول
					</th>
					<th>
						قیمت
						<br>
						(تومان)
					</th>
				</tr>
				</thead>
				<tbody>
				<tr v-for="x in selectedProductsBox">
					<td>
						<div class="btn-group-xs btn-group-vertical" style="width: 20px">
						  	<button v-on:click="addToCart(x)" class="btn btn-success">+</button>
							<button v-on:click="removeCart(x)" class="btn btn-danger">-</button>
						</div>
						{{ x.number | persian_digits}} 
					</td>
					<td>
						<small>
						{{ x.bastebandi }}
						{{ x.toharbastebandi }} تایی
						</small>
						<br>
						{{ x.name }}
					</td>
					<td>
						{{ x.price * x.number | persian_digits}}
					</td>
				</tr>
				</tbody>
				</table>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<button v-on:click="finish()" class="btn btn-primary btn-block">آدرس‌دهی
				</button>
				<div class="half-seperate"></div>
			</div>
		</div>
		<dl class="dl-horizontal">
		  	<dt class="hidden-xs">جمع کل هزینه ها</dt>
		  	<dd class="hidden-xs">{{totalprice | persian_digits}} تومان</dd>
		  	<dt v-if="off < 1">جمع هزینه ها پس از تخفیف</dt>
		  	<dd v-if="off < 1">{{totalprice * off | persian_digits}} تومان</dd>
		  	<dt>هزینه ارسال</dt>
		  	<dd>{{ peyk | persian_digits}} تومان</dd>
		  	<div class="half-seperate"></div>
		  	<dt class="big-size">هزینه قابل پرداخت</dt>
		  	<dd class="big-size">{{totalprice * off + peyk | persian_digits}} تومان</dd>
		</dl>
	</div>
	</div>`,
	props: {
    	type: {
      		type: String,
      		required: true
    	},
    	userId: {
    		type: Number,
      		required: true
    	},
    	off: {
    		type: Number,
      		required: true
    	},
    	peyk: {
    		type: Number,
      		required: true
    	},
  	},
	data: function () {
		return {
			Totalprice: 0,
			products: [],
			selectedproducts: [],
			selectedProductsId: [],
			productName: '',
			loading: true,
			types:[],
			selectedType:0,
			productPrice:100,
		}
	},
	methods: {
		fetchData: function () {
			this.$http.get('/api/type/'+this.type).then(function (response) {
				this.products = response.data.products;
				this.types = response.data.types;
				this.selectedType = this.types[0].id;
				this.loading = false;
			});
			this.$http.get('/api/order/'+this.userId).then(function (response) {
				this.selectedproducts = response.data.products;
				this.selectedProductsId = response.data.product_ids;
			});
		},
		addToCart: function (product) {
			this.selectedproducts.push(product);
			this.selectedProductsId.push(product.id);
			this.$http.post('/api/order/'+this.type, {product_ids: this.selectedProductsId, 
				user_id:  this.userId ? this.userId : 0  } );
		},
		removeCart: function (product) {
			var index = this.selectedproducts.indexOf(product);
    		this.selectedproducts.splice(index, 1);
			var index = this.selectedProductsId.indexOf(product.id);
    		this.selectedProductsId.splice(index, 1);
    		this.$http.post('/api/order/'+this.type, {product_ids: this.selectedProductsId,
    		user_id: this.userId ? this.userId : 0  } );
		},
		finish: function () {
			if(this.totalprice >  1){
				this.$http.post('/api/order/'+this.type, {product_ids: this.selectedProductsId, 
				user_id:  this.userId ? this.userId : 0 } ).then(function (response) {
					window.location.assign("/address/"+response.data);
				});
			}else{
				alert('شما از حداقل سفارش ' + 1000  +' تومان کمتر خرید کرده‌اید.')
			}
		},
		scrollType: function(id){
			var type = "#type" + id ;
			$('html,body').animate({
	        	scrollTop: $(type).offset().top
	        },'slow');
		},
		numberProduct :function(productName) {
			number = 0;
			this.selectedProductsBox.forEach(function(item){
				if(item.id == productName){
					number = item.number;
				}
			});
			return number;
		}
		
	},
	mounted: function () {
		this.fetchData();
	},
 	computed: {
		selectedProductsBox: function() {
			var uniqueNames = [];
			var uniqueproducts = [];
			var withNumber = this.selectedproducts;
			$.each(withNumber, function(index, element){
	    		element.number = 1;
	    	});
			$.each(withNumber, function(index, element){
			    if($.inArray(element.id, uniqueNames) === -1){
		    		uniqueNames.push(element.id);
		    		uniqueproducts.push(element);
		    	}else{
		    		uniqueproducts[$.inArray(element.id, uniqueNames)].number ++;
		    	}
			});
			return uniqueproducts.reverse();
		},
		totalprice:function() {
			var price = 0;
			$.each(this.selectedproducts, function(index, element){
	    		price += element.price;
	    	});
	    	return price;
		},
	},
});

Vue.component('choose-address', {
  	template: `
  	<div>
  	<p class="bold double-size text-center">آدرس خود را مشخص نمایید.
  	<a v-if="selectedAddress" v-on:click="finish()" style="cursor:pointer"><br> مراجعه به صندوق </a>
  	</p>
  	<div class="radio-style">
	    <div class="radio-button" v-for="(address, index) in addresses">
	    	<input type="radio" v-bind:value="address" v-model="selectedAddress" v-bind:id="index" >
	    	<label v-bind:for="index">
		    	<span class="radio">
		    		آدرس: {{ address.name }} 
		    		<div class="half-seperate"></div>
		    		{{ address.reciever }} - {{ address.phone }}
		    		<span v-if="address.postal_code">		    		
		    		{{ address.sabet }}
		    		</span>
		    		<span v-if="address.postal_code"> 
		    		کدپستی‌:
		    		{{ address.postal_code}}
		    		</span>
		    	</span>
	    	</label>
	    </div>
	</div>
	<div class="seperate"></div>
	<label class="visible-xs">
	اگر توضیح دیگری دارید وارد نمایید:
	</label>
	<input type="text" v-model="description" placeholder="مثلا نوشابه مشکی باشد" class="form-control visible-xs">

	<div class="one-third-seperate"></div>
	<div class="shopping-card" v-bind:style="selectedAddress ? 'background-color: #afa' : 'background-color: #f74'"  
	style="max-width: 400px">	
		<label v-if="!selectedAddress">
			لطفا یکی از آدرس‌های خود را انتخاب نمایید یا آدرس جدید وارد کنید:
		</label>
		<div v-if="selectedAddress" >
			<label>
				آدرس دریافتی شما:
			</label>
			<p>
				{{selectedAddress.name}}
			</p>
			<label class="hidden-xs">
				اگر توضیح دیگری دارید وارد نمایید:
			</label>
			<input  type="text" v-model="description" placeholder="مثلا نوشابه مشکی باشد" class="form-control hidden-xs">
			<div class="half-seperate"></div>
			<button v-on:click="finish()" class="btn btn-success double-size btn-block"
			style="background:#95a"> مراجعه به صندوق </button> 
		</div>
	</div>
  	</div>`,
	props: {
    	segment2: {
      		type: String,
      		required: true
    	},
    	userId: {
    		type: Number,
      		required: true
    	},
  	},
	data: function () {
		return {
			selectedAddress: '',
			addresses: [],
			inputAddress: '',
			description : ''
		}
	},
	methods: {
		fetchData: function () {
			this.$http.get('/api/address/' + this.userId).then(function (response) {
				this.addresses = response.data;
				this.selectedAddress = this.addresses[0];
			});
		},
		finish: function () {
			this.$http.post('/api/address/' + this.segment2, 
				{ address: this.selectedAddress.id , description: this.description} ).then(
				function (response) {
					window.location.assign("/checkout/" + this.segment2);
				});
		},
	},
	mounted: function () {
		this.fetchData();
	}
});



// Vue.use(VueLazyload, {
//   preLoad: 1.3,
//   error: 'dist/error.png',
//   loading: 'dist/loading.gif',
//   attempt: 1
// });
Vue.filter('persian_digits', function (value) {
    var i;
    var english_numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    var persian_numbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
    var output = commafy(Math.round(value) ).toString();
    for (i = 0; i < 10; i++) {
        output = output.replace(new RegExp(english_numbers[i], "gim"), persian_numbers[i]);
    }
    return output;
}); 
new Vue({
    el: '#body_id'
});
function commafy( num ) {
    var str = num.toString().split('.');
    if (str[0].length >= 4) {
        str[0] = str[0].replace(/(\d)(?=(\d{3})+$)/g, '$1,');
    }
    if (str[1] && str[1].length >= 5) {
        str[1] = str[1].replace(/(\d{3})/g, '$1 ');
    }
    return str.join('.');
}