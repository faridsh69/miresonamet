@extends('layout.master')
@section('fluid-container')
<div class="admin">
	<div class="box-nav">
		<div class="box-nav-inner">
			<div class="title">
				<a href="/admin/dashboard" class="bold {{ Request::segment(2) == 'dashboard' ? 'selected':'' }}">
					<span class="glyphicon glyphicon-cog"></span>
					داشبورد
				</a>
			</div>
			<div class="items">
				<div class="item">
					<a href="javascript:void(0)" class="bold"><span class=""></span>کاربران</a>
					<div class="sub-item">
						<a href="/admin/profile" class="{{ Request::segment(2) == 'profile' ? 'selected':'' }}"><span class="glyphicon glyphicon-user"></span> پروفایل کاربری</a>
						<a href="/admin/change-password" class="{{ Request::segment(2) == 'change-password' ? 'selected':'' }}"><span class="glyphicon glyphicon-lock"></span> تغییر رمز عبور</a>
						<a href="/admin/charge-credit" class="{{ Request::segment(2) == 'charge-credit' ? 'selected':'' }}"><span class="glyphicon glyphicon-usd"></span> افزایش اعتبار</a>
						<a href="/admin/my-order" class="{{ Request::segment(2) == 'my-order' ? 'selected':'' }}"><span class="glyphicon glyphicon-shopping-cart"></span> مسیرهای من</a>
						<!-- <a href="/admin/my-favorite" class="{{ Request::segment(2) == 'my-favorite' ? 'selected':'' }}"><span class="glyphicon glyphicon-credit-card"></span> موردعلاقه های من</a> -->
						<a href="/admin/logout" style="color: red"><span class="glyphicon glyphicon-off"></span> خروج</a>
					</div>
				</div>
				@can('secretary')
				<!-- <div class="item">
					<a href="javascript:void(0)" class="bold" >منشی</a>
					<div class="sub-item">
						<a href="/admin/order/manage" class="{{ Request::segment(2) == 'order' ? 'selected':'' }}"><span class="glyphicon glyphicon-shopping-cart"></span>سفارشات</a>
						<a href="/admin/product" class="{{ Request::segment(2) == 'product' ? 'selected':'' }}"><span class="glyphicon glyphicon-book"></span> محصولات</a>
						<a href="/admin/type" class="{{ Request::segment(2) == 'type' ? 'selected':'' }}"><span class="glyphicon glyphicon-tags"></span> دسته بندی ها</a>
					</div>
				</div> -->
				@endcan
				@can('manager')
				<div class="item">
					<a href="javascript:void(0)" class="bold" >مدیریت</a>
					<div class="sub-item">
						<!-- <a href="/admin/shop" class=" {{ Request::segment(2) == 'shop' ? 'selected':'' }}"><span class="glyphicon glyphicon-signal"></span> فروشگاه من</a> -->
						<a href="/admin/report" class=" {{ Request::segment(2) == 'report' ? 'selected':'' }}"><span class="glyphicon glyphicon-signal"></span> گزارشات</a>
						<!-- <a href="/admin/payment/manage" class="{{ Request::segment(2) == 'payment' ? 'selected':'' }}"><span class="glyphicon glyphicon-credit-card"></span> پرداخت ها</a> -->
						<a href="/admin/user/manage" class="{{ Request::segment(2) == 'user' && Request::segment(3) == 'manage' ? 'selected':'' }}"><span class="glyphicon glyphicon-user"></span> کاربران</a>
						<!-- <a href="/admin/notice/manage" class="{{ Request::segment(2) == 'notice' ? 'selected':'' }}"><span class="glyphicon glyphicon-send"></span> اطلاع رسانی</a> -->
						@can('developer')
						<!-- <a href="/admin/log" class="{{ Request::segment(2) == 'log' ? 'selected':'' }}"><span class="glyphicon glyphicon-barcode"></span> مشاهده لاگ</a> -->
						<a href="/admin/comment" class="{{ Request::segment(2) == 'comment' ? 'selected':'' }}"><span class="glyphicon glyphicon-comment"></span> نظرات</a>
						@endcan
					</div>
				</div>
				@endcan
			</div>
		</div>
	</div>
	<div class="box-container">
		<div class="content">
			<div style="height:50px"></div>
			@yield('content')
			<div style="height:20px"></div>
		</div>
	</div>
</div>

@endsection