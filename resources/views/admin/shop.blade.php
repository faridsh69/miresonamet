@extends('admin.dashboard')
@section('content')
<!--  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>

<select class="selectpicker" data-style="btn-info" multiple data-max-options="3" data-live-search="true">
    <optgroup label="Web">
        <option>PHP</option>
        <option>CSS</option>
        <option>HTML</option>
        <option>CSS 3</option>
        <option>Bootstrap</option>
        <option>JavaScript</option>
    </optgroup>
    <optgroup label="Programming">
      <option>Java</option>
      <option>C#</option>
      <option>Python</option>
    </optgroup>
  </select>
   -->
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" method="POST" class="table-lable">
		{{ csrf_field() }}
		<button type="submit" class="btn btn-success btn-block"> ذخیره اطلاعات </button>
		<div class="seperate"></div>
		<div class="row">
			<div class="col-xs-12">
			@if ($errors->all())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
                </ul>
            </div>
        	@endif
			</div>
		</div>
		@foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
        <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
                <li>{{ Session::get('alert-' . $msg) }}</li>
            </ul>
        </div>
        @endif
        @endforeach
		<div class="panel panel-info">
			<div class="panel-heading">تنظیمات فروشگاه</div>
            <div class="table-responsive">
			<table class="table table-striped table-hover">				
				<tr>
					<td>میزان تخفیف</td>
					<td><input type="number" name="off" class="form-control" min="0" max="99" 
					value="{{ $shop->off or old('off') }}">
					</td>
				</tr>
				<tr>
					<td>مبلغ پیک موتوری</td>
					<td><input type="number" name="peyk_in" class="form-control" min="0" max="4000"	value="{{ $shop->peyk_in  or old('peyk_in') }}" required>
				</tr>
				<tr>
					<td>حداقل هزینه سفارش از رستوران</td>
					<td><input type="number" name="minimum_price" class="form-control" min="3000" max="15000"
					value="{{ $shop->minimum_price  or old('minimum_price') }}" required>
					<div class="help-block">قراردادن رقم حدود ۵-۷ معقول است.</div></td>
					<input type="hidden" name="user_id"	value="{{ $shop->user_id or \Auth::id() }}">
				</tr>
				<tr>
					<td>شماره کارت بانک</td>
					<td><input type="text" name="credit_card" class="form-control"
					value="{{ $shop->credit_card  or old('credit_card') }}" required>
					<div class="help-block">مثال :  ۱۰۰۰-۲۰۰۰-۴۰۰۰-۸۰۰۰ </div></td>
				</tr>
				<tr>
					<td>ساعت کاری</td>
					<td>
						<label class="col-xs-3">شروع دریافت سفارش از ساعت</label>
						<input type="number" name="start_time" class="form-control col-xs-3" min="0" max="24"
						value="{{ $shop->start_time  or old('start_time') }}" placeholder="12" required>
						<label class="col-xs-1">الی</label>
						<input type="number" name="end_time" class="form-control col-xs-3" min="0" max="24"
						value="{{ $shop->end_time  or old('end_time') }}"	placeholder="23" required>
						<div class="half-seperate"></div>
						<div class="help-block">این فیلد بیانگر زمان‌هایی است که مشتری می تواند سفارش دهد.</div>
					</td>
				</tr>		
				
				<tr>
					<td>آپلود عکس 
					<div class="help-block">حجم تصویر غذا حداکثر ۲۰۰ کیلوبایت باشد.</div>
					</td>
					<td>
						<input type='file' accept='image/*' name="image_id" onchange='uploadImage(event)'>
						<div class="text-center">
						<img id='preview' class="img-responsive img-thumbnail">
						@if(isset($shop))
						عکس سابق
						@if($shop->image_id)
						<img src="/storage/shop/{{$shop->image_id}}-{{$shop->image->name}}"  class="img-responsive img-thumbnail">
						@else
						 ندارد
						@endif
						@endif
						<p id='no-preview'>
							<span class="glyphicon glyphicon-camera"></span>
							<br/>
							پیش‌نمایش تصویر
						</p>
						</div>
					</td>
				</tr>				
			</table>			
        	</div>
		</div>
		<div class="panel panel-info">
			<div class="panel-heading">اطلاعات تماس</div>
            <div class="table-responsive">
			<table class="table table-striped table-hover">				
				<tr>
					<td>شماره همراه</td>
					<td><input type="text" name="phone" class="form-control"
					value="{{ $shop->phone  or old('phone')  }}" required></td>
				</tr>
				<tr>
					<td>ایمیل</td>
					<td><input type="email" name="email" class="form-control"
					value="{{ $shop->email  or old('email')  }}" required></td>
				</tr>
			    <tr>
				    <td for="province">استان</td>
				    <td>
						<select id="province" class="form-control" name="province">
							@foreach(\App\Models\Province::get() as $province)
							<option value="{{ $province->id }}">
								{{ $province->name }}
							</option>
							@endforeach
						</select>	
					</td>	    
				</tr>
			    <tr>
				    <td for="city">شهر</td>
				    <td>
				      	<select id="city" class="form-control" name="city">
						</select>	
					</td>
			    </tr>
				<tr>
					<td>آدرس</td>
					<td>
					<textarea class="form-control" name="address">{{ $shop->address->name  or old('address') }}</textarea>
				</tr>
				<tr>
					<td>آدی تلگرام</td>
					<td><input type="text" name="telegram" class="form-control" 
					value="{{ $shop->telegram or old('telegram') }}">
					</td>
				</tr>
				<tr>
					<td>آدی اینستاگرام</td>
					<td><input type="text" name="instagram" class="form-control" 
					value="{{ $shop->instagram or old('instagram') }}">
					</td>
				</tr>
				<tr>
					<td>درباره ما</td>
					<td>
					<textarea class="form-control" name="about">{{ $shop->about  or old('about') }}</textarea>
				</tr>
			</table>			
        	</div>
		</div>
		<div class="panel panel-warning">
			<div class="panel-heading">
				 تنظیمات صفحات سایت 
			</div>
            <div class="table-responsive">
			<table class="table table-striped">
				<tr>
					<td>نام فروشگاه (فارسی)</td>
					<td><input type="text" name="name" class="form-control"
					value="{{ $shop->name  or old('name')  }}" required></td>
				</tr>
				<tr>
					<td>نام فروشگاه (انگلیسی)</td>
					<td><input type="text" name="name_en" class="form-control"
					value="{{ $shop->name_en  or old('name_en')  }}" required></td>
				</tr>				
				
				<tr>
					<td>عنوان پیش‌فرض صفحات</td>
					<td><input type="text" name="title" class="form-control"
					value="{{ $shop->title  or old('title')  }}" required></td>
				</tr>
				<tr>
					<td>کیورد های موتور جستجو</td>
					<td><input type="text" name="keyword" class="form-control"
					value="{{ $shop->keyword  or old('keyword')  }}" required></td>
				</tr>
				<tr>
					<td>توضیحات موتور جستجو</td>
					<td><input type="text" name="description" class="form-control"
					value="{{ $shop->description  or old('description')  }}" required></td>
				</tr>	
				<tr>
					<td>تعداد ایتم در هر صفحه</td>
					<td><input type="number" name="page_size" class="form-control" min="4" max="100" value="{{ $shop->page_size or old('page_size') }}" required>
					</td>
				</tr>
			</table>
			</div>
		</div>
		<div class="panel panel-danger">
			<div class="panel-heading">پنل درگاه بانک و پیامک و ایمیل</div>
            <div class="table-responsive">
			<table class="table table-striped table-hover">				
				<tr>
					<td>نام کاربری پنل اس ام اس</td>
					<td><input type="text" name="sms_user" class="form-control"
					value="{{ $shop->sms_user or old('sms_user')  }}" required></td>
				</tr>
				<tr>
					<td>شماره درگاه اس ام اس</td>
					<td><input type="text" name="sms_phone" class="form-control"
					value="{{ $shop->sms_phone or old('sms_phone')  }}" required></td>
				</tr>
				<tr>
					<td>پسورد درگاه اس ام اس</td>
					<td><input type="text" name="sms_pass" class="form-control"
					value="{{ $shop->sms_pass or old('sms_pass')  }}" required></td>
				</tr>
				<tr>
					<td>شماره درگاه زرین پال</td>
					<td><input type="text" name="merchant_code_zarinpal" class="form-control" 
					value="{{ $shop->merchant_code_zarinpal or old('merchant_code_zarinpal') }}">
					</td>
				</tr>
				<tr>
					<td>نام کاربری ایمیل</td>
					<td><input type="email" name="email_user" class="form-control" 
					value="{{ $shop->email_user or old('email_user') }}">
					</td>
				</tr>
				<tr>
					<td>پسورد ایمیل</td>
					<td><input type="text" name="email_pass" class="form-control" 
					value="{{ $shop->email_pass or old('email_pass') }}">
					</td>
				</tr>
			</table>			
        	</div>
		</div>
		<button type="submit" class="btn btn-success btn-block"> ذخیره اطلاعات </button>
		<div class="seperate"></div>
		</form>
	</div>
</div>
@endsection
@push('script')
<script type="text/javascript">
    select = document.getElementById('city');
	$("#province").change(function () {
		getCity(this.value);
  	});
	function getCity(ostan)
	{
		$.get('/api/city/'+ostan, function( data ) {
		  	for (var i=0; i<15; i++){
	     		select.remove(0);
		  	}
			data.forEach(function(item){
			    var opt = document.createElement('option');
			    opt.value = item.id;
			    opt.innerHTML = item.name;
			    select.appendChild(opt);				
			});
		});
	}
	getCity(1);
</script>

@endpush