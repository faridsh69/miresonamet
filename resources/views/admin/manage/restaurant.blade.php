@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
		<div class="panel panel-default">
		<div class="panel-heading">مدیریت رستوران‌ها</div>
		<div class="half-seperate"></div>
		<form class="form-inline" method="GET">
		  	<div class="form-group">
		    	<label for="name">نام رستوران:</label>
		   	 	<input type="text" class="form-control input-sm" id="name" name="name">
		  	</div>
		  	<button type="submit" class="btn btn-default input-sm">جستجو</button>
		</form>
		<div class="half-seperate"></div>
		<div class="table-responsive">
		<table class="table">
		<thead>
		<tr>
			<th>
			شماره
			</th>
			<th>
			نام رستوران
			</th>
			<th>
			شهر
			</th>
			<th>
			آدرس
			</th>
			<th>
			میزان تخفیف
			</th>
			<th>
			ساعت کاری	
			</th>
			<th>
			نوع غذاها
			</th>
			<th>
			شماره همراه
			</th>
			<th>
			پیک 
			</th>
			<th>
			حداقل هزینه 
			</th>
			<th>
			ویرایش
			</th>
			<th>
			غذاها
			</th>
			<th>
			سفارشات
			</th>
			<th>
			گزارشات
			</th>
		</tr>
		</thead>
		<tbody>
		@foreach($restaurants as $restaurant)
		<tr>
			<td>
			{{ $restaurant->id }}
			</td>
			<td>
			{{ $restaurant->name }}
			</td>
			<td>
			{{ $restaurant->city->name }}
			</td>
			<td>
			{{ $restaurant->address }}
			</td>
			<td>
			{{ \Nopaad\Persian::correct( $restaurant->off ) }}
			</td>
			<td>
			{{ \Nopaad\Persian::correct( $restaurant->start_time )}}
			-
			{{ \Nopaad\Persian::correct( $restaurant->end_time ) }}
			</td>
			<td>
			@foreach( $restaurant->types as $type )
			{{ $type->name }} | 
			@endforeach
			</td>
			<td>
			{{ \Nopaad\Persian::correct( $restaurant->phone) }}
			</td>
			<td>
			{{ \Nopaad\Persian::correct( $restaurant->peyk_in) }} 
			</td>
			<td>
			{{ \Nopaad\Persian::correct( $restaurant->minimum_price) }} 
			</td>
			<td>
				<a href="/admin/restaurant/my-restaurant/{{$restaurant->id}}-{{$restaurant->name}}" 
				 class="btn btn-info">
				ویرایش
				</a>
			</td>
			<td>
				<a href="/admin/restaurant/my-food/{{$restaurant->id}}-{{$restaurant->name}}" 
				class="btn btn-warning">
				غذاها
				</a>
			</td>
			<td>
				<a href="/admin/order/manage?name={{$restaurant->name}}" class="btn btn-primary">
				سفارشات
				</a>
			</td>
			<td>
				<a href="/admin/report/manage?name={{$restaurant->name}}" class="btn btn-danger">
				گزارشات
				</a>
			</td>
		</tr>
		@endforeach
		</tbody>
		</table>
		</div>
		</div>
	</div>
</div>
@endsection