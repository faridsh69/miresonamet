@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
	@if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" action="" method="POST">
			{{ csrf_field() }}
			<div class="panel panel-default">
			<div class="panel-heading">پروفایل کاربر</div>
            <div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
					<td width="110px;">نام</td>
					<td><input type="text" name="first_name" class="form-control"
					value="{{ old('first_name' , \Auth::user()->first_name) }}"></td>
				</tr>
				<tr>
					<td>نام خانوادگی</td>
					<td><input type="text" name="last_name" class="form-control"
					value="{{ old('last_name' , \Auth::user()->last_name) }}"></td>
				</tr>
				<tr>
					<td>ایمیل</td>
					<td><input type="email" name="email" class="form-control"
					value="{{ old('email' , \Auth::user()->email) }}">
					<div class="help-block">برای اطلاع‌رسانی بیشتر</div></td>
				</tr>
				<tr>
					<td>تلفن همراه</td>
					<td><input type="text" name="phone" class="form-control"
					value="{{ old('phone' , \Auth::user()->phone) }}"></td>
				</tr>
				
				<tr>
					<td colspan="2">
					<button type="submit" class="btn btn-success btn-block"> ذخیره تغییرات </button>
					</td>
				</tr>
			</table>			
        	</div>
			</div>
		</form>
		<form enctype="multipart/form-data" action="/admin/charge-credit" method="POST">
			{{ csrf_field() }}
			<div class="panel panel-info">
				<div class="panel-heading">آدرس های شما</div>
				@if( count(\Auth::user()->addresses) == 0 )
					<div class="help-block"> آدرسی تاکنون وارد نکره‌اید. </div>
				@else
			    		<div class="seperate"></div>

				<ol>
				@foreach(\Auth::user()->addresses as $address)
					<li>
						{{ $address->name }}
			    		<div class="one-third-seperate"></div>
			    		{{  $address->reciever }} - {{ $address->phone }}
			    		@if($address->sabet)
			    		<span> -				    		
			    		{{ $address->sabet }}
			    		</span>
			    		@endif
			    		@if($address->postal_code)
			    		<span> 
			    		کدپستی‌:
			    		{{ $address->postal_code}}
			    		</span>
			    		@endif
			    		<hr>
					</li>
				@endforeach
				@endif
				</ol>
            	<div class="form-horizontal">
					<h3 class="text-center page-header">ثبت آدرس جدید</h3>
					<div class="form-group">
					    <label for="name" class="col-sm-3 control-label">نام و نام‌خانوادگی تحویل گیرنده</label>
					    <div class="col-sm-3">
					      	<input type="text" class="form-control" id="name" name="name" value="{{Auth::user()->first_name}} {{Auth::user()->last_name}}" required>
					    </div>
					    <label for="phone" class="col-sm-2 control-label">شماره تماس ضروری (همراه)</label>
					    <div class="col-sm-3">
					      	<input type="text" class="form-control" id="phone" name="phone" value="{{Auth::user()->phone}} " required>
					    </div>
				  	</div>
				  	<div class="form-group">
					    <label for="sabet" class="col-sm-3 control-label">شماره تلفن ثابت تحویل گیرنده (اختیاری)</label>
					    <div class="col-sm-3">
					      	<input type="text" class="form-control" id="sabet" name="sabet">
					    </div>
					    <label for="postcode" class="col-sm-2 control-label">کد پستی (اختیاری)</label>
					    <div class="col-sm-3">
					      	<input type="text" class="form-control" id="postcode" name="postcode">
					    </div>
				  	</div>
				  	<div class="form-group">
					    <label for="province" class="col-sm-3 control-label">استان</label>
					    <div class="col-sm-2">
							<select id="province" class="form-control" name="province">
								@foreach(\App\Models\Province::get() as $province)
								<option value="{{ $province->id }}">
									{{ $province->name }}
								</option>
								@endforeach
							</select>		    
						</div>
					    <label for="city" class="col-sm-1 control-label">شهر</label>
					    <div class="col-sm-2">
					      	<select id="city" class="form-control" name="city">
							</select>	
					    </div>
					    <label for="state" class="col-sm-1 control-label">منطقه</label>
					    <div class="col-sm-2">
					      	<select id="state" class="form-control" name="state">
								<option value="کل شهر">
									کل شهر
								</option>
							</select>	
					    </div>
				  	</div>
				  	<div class="form-group">
					    <label for="addres" class="col-sm-offset-1 col-sm-2 control-label">آدرس پستی</label>
					    <div class="col-sm-8">
					    	<textarea type="text" id="addres" class="form-control" name="address" required></textarea>
					    </div>
				  	</div>
				</div>
				<div class="row">
					<div class="col-sm-8 col-sm-offset-3">
						<button class="btn btn-success btn-block" type="submit"> ذخیره آدرس</button>
					</div>
				</div>
				<div class="seperate"></div>
			</div>
		</form>
	</div>
</div>
<div class="seperate"></div>
@endsection

@push('script')
<script type="text/javascript">
    select = document.getElementById('city');
	$("#province").change(function () {
		getCity(this.value);
  	});
	function getCity(ostan)
	{
		$.get('/api/city/'+ostan, function( data ) {
		  	for (var i=0; i<15; i++){
	     		select.remove(0);
		  	}
			data.forEach(function(item){
			    var opt = document.createElement('option');
			    opt.value = item.id;
			    opt.innerHTML = item.name;
			    select.appendChild(opt);				
			});
		});
	}
	getCity(1);
</script>

@endpush