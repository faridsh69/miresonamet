@extends('admin.dashboard')
@section('content')
<div class="row">
	<div class="col-xs-12">
	@foreach (['danger', 'warning', 'success', 'info'] as $msg)
        @if(Session::has('alert-' . $msg))
            <div class="alert alert-{{ $msg }} alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <ul class="list-unstyled">
                    <li>{{ Session::get('alert-' . $msg) }}</li>
                </ul>
            </div>
        @endif
    @endforeach
	@if ($errors->all())
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <ul class="list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
            </ul>
        </div>
    @endif
	</div>
</div>
<div class="row">
	<div class="col-xs-12">
		<form enctype="multipart/form-data" action="/admin/change-password" method="POST">
			{{ csrf_field() }}
			<div class="panel panel-default">
			<div class="panel-heading">	تغییر رمز عبور</div>
            <div class="table-responsive">
			<table class="table table-striped table-hover">
				<tr>
					<td width="140px">رمز عبور قدیم:</td>
					<td><input type="text" name="oldpassword" class="form-control"></td>
				</tr>
				<tr>
					<td>رمز عبور جدید</td>
					<td><input type="text" name="newpassword" class="form-control"></td>
				</tr>
				<tr>
					<td colspan="2">
					<button type="submit" class="btn btn-danger btn-block">  تغییر رمز</button>
					</td>
				</tr>
			</table>			
        	</div>
			</div>
		</form>
	</div>
</div>
<div class="seperate"></div>
@endsection
