@extends('layout.master')
@section('container')
<div class="row text-center" style="background-color: white">
    <div class="col-sm-6" style="background-image: url('/public/img/map2.png');
background-position: cover;background-repeat: no-repeat;">            
        <div class="seperate"></div>
        <div class="seperate"></div>
        <img src="/public/img/start.png" style="position: absolute;top: 20px;left: 300px;width: 50px;">
        <span style="position: absolute;top: 34px;left: 297px;width: 50px;font-size: 10px">مقصد </span>
        <img src="/public/img/end.png" style="position: absolute;top: 510px;left: 347px;width: 50px;">
        <span style="position: absolute;top: 524px;left: 345px;width: 50px;font-size: 10px">مبدأ</span>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <img src="/public/img/end.png" style="visibility: hidden;">
    </div>
    <div class="col-sm-4 col-sm-offset-1 text-right" >
    	<div class="seperate"></div>
    	<div class="seperate"></div>
    	<div class="panel panel-info" style="background-color: white">
			<div class="panel-heading">
				مسیر شما
			</div>
	    	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-xs-12 big-size">
		    			<button class="btn btn-info btn-xs">
		    				<span class="glyphicon glyphicon-pencil"></span>
		    			</button>
		    			<label>
		    				<span class="glyphicon glyphicon-map-marker"></span>مبدأ:</label>
		    			<span style="font-size: 10px">میدان صادقیه، خیابان آیت الله کاشانی ، شهرداری منطقه ۵</span>
		    			<hr style="margin-top: 0px;">
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-xs-12 big-size">
		    			<button class="btn btn-info btn-xs">
		    				<span class="glyphicon glyphicon-pencil"></span>
		    			</button>
		    			<label>
		    				<span class="glyphicon glyphicon-map-marker"></span>مقصد:</label>
		    			<span style="font-size: 10px">بزرگراه شهید ستاری شمال، نبش خیابان پیامبر مرکزی، مجتمع تجاری کوروش</span>
		    			<hr style="margin-top: 0px;">
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-xs-11 col-xs-offset-1 hamlonaghl">
		    			<label>
		    			وسایل حمل و نقل:
		    			</label>
		    			<i class="fa fa-subway"></i>
		    			<i class="fa fa-bus"></i>
		    			<i class="fa fa-male"></i>
		    			<i class="fa fa-cab"></i>
		    			<br>
		    			<label>
		    			اولویت:
		    			</label>
		    			کمترین هزینه
		    		</div>
		    	</div>
		    </div>
		</div>
    	<div class="row">
    		<div class="col-xs-12">
    			<label>
    				مسیر ۱:
    			</label>
    			<p>
    				۶۰۰ متر پیاده‌روی در امتداد خیابان آیت الله کاشانی به سمت غرب - ۶۰۰ متر به سمت شمال در راستای اتوبان ستاری
    			</p>
    			<span class="bold">
    				<i class="fa fa-tachometer"></i>
    				۱.۲ کیلومتر - 
    				<i class="fa fa-dollar"></i>
    				هزینه ۰ تومان
    				<span class="glyphicon glyphicon-time"></span>
    				۳۳ دقیقه
    			</span>
    		</div>
    		<div class="seperate"></div>
    	</div>
    	
    	<div class="row">
    		<div class="col-xs-12">
    			<label>
    				مسیر ۳:
    			</label>
    			<p>
    				۱۰۰ متر پیاده روی به سمت غرب کنید - سپس با تاکسی ستاری شمال را رفته و جلوی درب مجتمع تجاری کوروش پیاده شوید
    			</p>
    			<span class="bold">
    				<i class="fa fa-tachometer"></i>
    				۰.۹ کیلومتر - 
    				<i class="fa fa-dollar"></i>
    				هزینه ۱،۰۰۰ تومان
    				<span class="glyphicon glyphicon-time"></span>
    				۱۵ دقیقه
    			</span>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-xs-12">
    			<label>
    				مسیر ۳:
    			</label>
    			<p>
    				از کارپینو درخاست تاکسی دهید.
    			</p>
    			<span class="bold">
    				<i class="fa fa-tachometer"></i>
    				۱.۹ کیلومتر - 
    				<i class="fa fa-dollar"></i>
    				هزینه ۵،۰۰۰ تومان
    				<span class="glyphicon glyphicon-time"></span>
    				۱۸ دقیقه
    			</span>
    		</div>
    		<div class="seperate"></div>
    	</div>
    </div>
</div>
@endsection