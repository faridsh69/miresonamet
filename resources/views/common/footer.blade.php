@if(Request::segment(1) != 'admin')
<div class="background-footer">
<div class="container">
    <div class="seperate"></div>
    <div class="row">
        <div class="col-sm-4 col-md-3 col-lg-3">
            <h4 class="page-header">
            {{ \App\Http\Controllers\Controller::NAME }}
            </h4>
        </div>
        <div class="col-xs-11 col-xs-offset-1 col-sm-offset-0 col-md-8 col-lg-9">
            <!-- <img src="/public/img/footer-map.png" class="img-responsive"> -->
            <!-- <div class="seperate"></div> -->
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-4 col-md-3">
            <ul class="list-unstyled">
                <li>
                    <a href="/type/1">صفحه اصلی</a>
                </li>
                <li>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#about-us-modal">درباره ما</a>
                </li>
                <li>
                    <a href="javascript:void(0)" data-toggle="modal" data-target="#contact-us-modal">تماس با ما</a>
                </li>
            </ul>
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-sm-8 col-md-6">
            <div class="half-separator"><!-- sep --></div>
        </div>
    	<div class="col-md-12 col-md-3 text-right">
            <div>{{ \App\Http\Controllers\Controller::NAME}} در شبکه‌های اجتماعی</div>
            <div class="half-seperate"></div>
            <div class="social">
                <a href="https://www.facebook.com/miresonamet">  </a>
                <a href="https://telegram.me/miresonamet">  </a>
                <a href="https://www.instagram.com/miresonamet">  </a>
                <a href="https://www.twitter.com/miresonamet">  </a>
                <a href="https://www.plus.google.com/miresonamet">  </a>
                <a href="https://www.linkden.com/miresonamet">  </a>
            </div>
            <div class="one-third-seperate"></div>
            <p>کلیه حقوق این سایت متعلق به {{ \App\Http\Controllers\Controller::NAME}}
            می‌باشد.</p>
            <p style="color: #444;font-size: 95%">
                نویسنده سایت :<a href="http://rotbeyek.ir/cv">تیم آی تی می‌رسونمت</a>
            </p>
        </div>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>
</div>
@endif