<nav class="navbar background-nav " 
{{ Request::segment(1) != 'admin' ? '' : 'data-spy=affix data-offset-top="0"' }}>
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="glyphicon glyphicon-menu-hamburger"></span>                           
            </button>
            <a class="navbar-brand {{Request::segment(1) == '' ? 'selected':''}}" href="/"> 
                <img src="/public/img/logo.png" alt="logo" style="width: 190px">
                <!-- <span>
                    می‌رسونمت
                </span> -->
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="/masir" class="draw2">مسیریابی</a></li>
                <li><a href="/masir2" class="draw2">نمونه نتایج</a></li>
                <li><a href="javascript:void(0)" data-toggle="modal" data-target="#about-us-modal" class="draw2">درباره ما</a></li>
                <li><a href="javascript:void(0)" data-toggle="modal" data-target="#contact-us-modal" class="draw2">تماس با ما</a></li>
                <!-- <form class="navbar-form navbar-left" style="margin: 15px;">
                    <div>
                        <input type="text" class="form-control search-bar" placeholder="جستجوی محصول..." id="search" autocomplete="off">
                        <div id="livesearch"></div>
                    </div>
                </form> -->
            </ul>
            <ul class="nav navbar-nav navbar-left">
                <div class="half-seperate"></div>
                <div class="one-third-seperate"></div>
            @if(empty(Auth::user()))
                <div class="half-seperate"></div>
                <a href="/user/register" class="google-a"><span class="glyphicon glyphicon-user"></span> ثبت نام</a>
                |
                <a href="/user/login" class="google-a"><span class="glyphicon glyphicon-log-in"></span> ورود</a>
            @else
                <a href="/admin/profile">
                <span class="glyphicon glyphicon-cog big-size"></span>
                {{ Auth::user() ? Auth::user()->first_name : ''}} 
                {{ Auth::user() ? Auth::user()->last_name : ''}}
                <label class="label label-default" style="margin:3px;padding: 0px 7px 0px 7px">
                اعتبار: {{ \Nopaad\Persian::correct(number_format( Auth::user()->credit , 0, '',',')) }} تومان
                </label>
                @if(\Auth::user()->roles()->count() > 0)
                <br>
                    @foreach(\Auth::user()->roles()->pluck('name') as $role)
                        <small style="position: relative;">
                            <span class="label {{ in_array($role ,['manager']) ? 'label-danger' : 'label-success' }}">{{ trans('roles.' . $role) }}</span>
                        </small>
                    @endforeach
                @endif
                </a>
            @endif
            </ul>
        </div>
    </div>
</nav>
@if(Request::segment(1) != 'admin')
<img src="/public/img/shadow-3.png" width="100%" class="background-nav-shadow">
@else
<div class="affix-fixer"></div>
@endif
<div class="modal fade" id="contact-us-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">تماس با {{ \App\Http\Controllers\Controller::NAME}}</h4>
            </div>
            <div class="modal-body">
                <h5>
                    تلفن پشتیبانی:
                    ۰۹۲۲۳۲۳۱۹۳۶                    
                </h5>
                <div class="half-seperate"></div>
                <h5>
                    پست الکترونیک:
                    info@miresonamet.ir
                </h5>
                <div class="half-seperate"></div>
                <h5>
                    آدرس:
                    منطقه پنج تهران - بزرگراه ستاری - مجتمع تجاری کوروش 
                </h5>
            </div>
        </div>
    </div>
</div>
<div class="bottom-nav" >
    پشتیبانی ۲۴ ساعته
    -
    شماره تماس  
    {{\App\Http\Controllers\Controller::PHONE}}
    - 
    تلگرام ما:
    <span class="ltr"></span>
    <span>{{ \App\Http\Controllers\Controller::NAME_EN}}@</span>
</div>
<div class="modal fade" id="about-us-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">درباره ما</h4>
            </div>
            <div class="modal-body">
                <h5>
                    با می‌رسونمت بهترین مسیر برای رفت و آمد را با توجه به اولویت‌های خود بدست آورید.
                <div class="half-seperate"></div>
                <div class="half-seperate"></div>
                <div class="half-seperate"></div>

                    تلفن پشتیبانی:
                    ۰۹۲۲۳۲۳۱۹۳۶                    
                </h5>
                <h5>
                    پست الکترونیک:
                    info@miresonamet.ir
                </h5>
                <h5>
                    آدرس:
                    منطقه پنج تهران - بزرگراه ستاری - مجتمع تجاری کوروش 
                </h5>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var livesearch = document.getElementById('livesearch');
    $('#search').keyup(function() {
        if(this.value != '')
        {
            makeSearch();
        }else{
            livesearch.style.display="none";    
        }
    });
    function makeSearch()
    {
        $.ajax({
            url: '/search/'+ $('#search')[0].value,
            success: function(data){
                livesearch.innerHTML=data;
                if(data){
                    livesearch.style.display="block";
                }
            }
        });
    }
    $('#search').blur(function(){
        setTimeout(function() {
            livesearch.style.display="none";
        }, 300);
        
    });
</script>