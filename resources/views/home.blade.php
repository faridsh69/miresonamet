@extends('layout.master')
@section('container')

<div class="row text-center" style="background-image: url('/public/img/a33.jpg');
background-position: cover">
    <div class="col-sm-7">            
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <h1 class="bold text-center hug-size" style="color: white" >
            می‌رسونمت
        </h1>
        <hr>
        <h3 style="color: white;text-align: center;">  
            مسیریاب هوشمند
        </h3>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <a href="masir" class="button button--moema">
            <span class="glyphicon glyphicon-location "></span>
            مسیریابی
        </a>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
    </div>
    <div class="col-sm-5">
        <!-- <img src="/public/img/011.png" alt="mobile" class="img-responsive"> -->
        <div class="seperate"></div>
        <div class="seperate"></div>
        <img src="/public/img/012.png" alt="mobile" class="img-responsive">
    </div>
</div>
<div class="row text-center" style="background-color: white">
	<div class="col-xs-10 col-xs-offset-1 ">			
	    <h1 class="bold page-header hug-size" >
			{{ \App\Http\Controllers\Controller::NAME}}
		</h1>
		<h3>
			همسفر شما با قابلیت‌های مسیریابی با اولویت‌های شما
		</h3>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
    </div> 
    <div class="col-sm-4">
        <img src="/public/img/why-3.png" width="70px" alt="مرحله ۱">
        <div class="half-seperate"></div>
        <h5 class="bold">
        سریعترین مسیر
        <div class="seperate"></div>
        <small>محصول ما همواره به شما کمک می‌کند در زودترین زمان ممکن به مقصد برسید.</small>
        </h5>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-2.png" width="70px" alt="مرحله ۲">
        <div class="half-seperate"></div>
        <h5 class="bold">
        کم‌ترافیک ترین مسیر
        <div class="half-seperate"></div>
        <small>در تمام مراحل سفارش از جمله انتخاب محصول و ثبت نام و آدرس دهی تنها به راحتی شما اندیشیدیم.</small>
        </h5>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-1.png" width="60px" alt="مرحله ۳">
        <div class="half-seperate"></div>
        <h5 class="bold">
            گارانتی مسیر دلخواه شما 
            <div class="half-seperate"></div>
        <small>مبلغ به عنوان مالیات بر ارزش افزوده و هزینه بسته بندی گرفته نمی شود و نیز تخفیف‌هایی داریم.</small>
        </h5>
    </div>
    <div class="seperate"></div>
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>
<!-- <div class="row text-center back-nice">
    <div class="col-xs-12">
        <h4>غرفه رستوران</h4>
    </div>
</div> -->

@endsection
@push('script')
<script src="/public/js/jquery-lazy.js"></script>
<script type="text/javascript">
$("img.lazy").lazyload({
    // event : "sporty"
    // threshold : 200
    // effect : "fadeIn"
});
</script>
<script>
    var left = 1400;
    function goRight(event) {
        left = left + 200;
        $('.reyli-item-box').stop().animate({
            scrollLeft: left
        }, 'slow','swing',stop(true));
    }
    function goLeft(event) {
        left = left - 200;
        $('.reyli-item-box').stop().animate({
            scrollLeft: left
        }, 'slow','swing',stop(true));
    }

</script>
@endpush

