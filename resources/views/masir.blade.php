@extends('layout.master')
@section('container')
<div class="row text-center" style="background-color: white">
    <div class="col-sm-6" style="background-image: url('/public/img/map.png');
background-position: cover;background-repeat: no-repeat;">            
        <div class="seperate"></div>
        <div class="seperate"></div>
        <img src="/public/img/start.png" style="position: absolute;top: 20px;left: 300px;width: 50px;">
        <span style="position: absolute;top: 34px;left: 297px;width: 50px;font-size: 10px">مقصد </span>
        <img src="/public/img/end.png" style="position: absolute;top: 510px;left: 330px;width: 50px;">
        <span style="position: absolute;top: 524px;left: 328px;width: 50px;font-size: 10px">مبدأ</span>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <img src="/public/img/end.png" style="visibility: hidden;">
    </div>
    <div class="col-sm-4 col-sm-offset-1 text-right" >
    	<div class="seperate"></div>
    	<div class="seperate"></div>
    	<div class="row">
    		<div class="col-xs-12 big-size">
    			<label>
    				<span class="glyphicon glyphicon-map-marker"></span>مبدأ:</label>
    			<span style="font-size: 13px">میدان صادقیه، خیابان آیت الله کاشانی ، شهرداری منطقه ۵</span>
    			<hr style="margin-top: 0px;">
    		</div>
    	</div>
    	<div class="seperate"></div>
    	<div class="row">
    		<div class="col-xs-12 big-size">
    			<label>
    				<span class="glyphicon glyphicon-map-marker"></span>مقصد:</label>
    			<span style="font-size: 12px">بزرگراه شهید ستاری شمال، نبش خیابان پیامبر مرکزی، مجتمع تجاری کوروش</span>
    			<hr style="margin-top: 0px;">
    		</div>
    	</div>
    	<div class="seperate"></div>
    	<div class="panel panel-default" style="background-color: white">
			<div class="panel-heading">
				فیلتر هوشمند
			</div>
	    	<div class="panel-body">
		    	<div class="row">
		    		<div class="col-xs-12">
		    			<div class="radio-style">
						    <div class="radio-button">
						    	<input type="radio" value="10" name="vasile" id="index" checked="true" >
						    	<label for="index">
							    	<span class="radio">
							    		<i class="fa fa-subway"></i>
							    		مترو
							    	</span>
						    	</label>
						    </div>
						</div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-xs-12">
		    			<div class="radio-style">
						    <div class="radio-button">
						    	<input type="radio" value="0" name="vasile2" id="vasile9" checked="false" >
						    	<label for="vasile9">
							    	<span class="radio">
							    		BRT
							    		اتوبوس تندرو 
							    	</span>
						    	</label>
						    </div>
						</div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-xs-12">
		    			<div class="radio-style">
						    <div class="radio-button">
						    	<input type="radio" value="10" name="vasile9" id="index" checked="true" >
						    	<label for="index">
							    	<span class="radio">
							    		<i class="fa fa-bus"></i>
							    	 	اتوبوس
							    	</span>
						    	</label>
						    </div>
						</div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-xs-12">
		    			<div class="radio-style">
						    <div class="radio-button">
						    	<input type="radio" value="10" name="vasile190" id="index" checked="true" >
						    	<label for="index">
							    	<span class="radio">
							    	 	<i class="fa fa-male"></i>
							    	 	پیاده
							    	</span>
						    	</label>
						    </div>
						</div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-xs-12">
		    			<div class="radio-style">
						    <div class="radio-button">
						    	<input type="radio" value="10" name="vasile2" id="index" checked="false" >
						    	<label for="index">
							    	<span class="radio">
							    	 	<i class="fa fa-motorcycle"></i>
							    	 	دوچرخه
							    	</span>
						    	</label>
						    </div>
						</div>
		    		</div>
		    	</div>
		    	
		    	<div class="row">
		    		<div class="col-xs-12">
		    			<div class="radio-style">
						    <div class="radio-button">
						    	<input type="radio" value="10" name="vasile2" id="index" checked="false" >
						    	<label for="index">
						    		
							    	<span class="radio">
							    		<i class="fa fa-cab"></i>
							    	 	تاکسی
							    	</span>
						    	</label>
						    </div>
						</div>
		    		</div>
		    	</div>
		    	<div class="row">
		    		<div class="col-xs-12">
		    			<label>
		    				اولویت:
		    			</label>
		    			<select class="form-control">
		    				<option  selected="true">کمترین هزینه</option>
		    				<option  >کمترین زمان</option>
		    				<option  >نزدیکترین مسیر</option>
		    			</select>
		    		</div>
		    	</div>
		    	<div class="seperate"></div>
		    	<div class="row">
		    		<div class="col-xs-6">
		    			<button class="btn btn-block btn-info">
		    				<span class="glyphicon glyphicon-menu-down	"></span>
		    			فیلترهای پیشرفته</button>
		    		</div>
		    		<div class="col-xs-6">
		    			<a href="/masir2" class="btn btn-block btn-success">
		    				<span class="glyphicon glyphicon-search"></span>
		    			جست‌جو</a>
		    		</div>
		    	</div>
		    </div>
		</div>
    </div>
</div>
@endsection