@extends('layout.master')
@section('container')

<div class="row text-center" style="background-image: url('/public/img/a33.jpg');
background-position: cover">
    <div class="col-sm-7">            
        <div class="seperate"></div>
        <h1 class="bold text-center hug-size" style="color: white" >
            می‌رسونمت
        </h1>
        <hr>
        <h3 style="color: white;text-align: center;">  
            مسیریاب هوشمند
        </h3>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <a href="register-idea" class="button button--moema">
            <span class="glyphicon glyphicon-location "></span>
            مسیریابی
        </a>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
        <div class="seperate"></div>
    </div>
    <div class="col-sm-5">
        <!-- <img src="/public/img/011.png" alt="mobile" class="img-responsive"> -->
        <img src="/public/img/012.png" alt="mobile" class="img-responsive">
    </div>
</div>
<div class="row" style="background-color: white">
    <div class="seperate"></div>
    <div class="seperate"></div>
</div>



<div class="row">
	<div class="col-xs-10 col-xs-offset-1 text-center">			
	    <h1 class="bold page-header hug-size" >
			{{ \App\Http\Controllers\Controller::NAME}}
		</h1>
		<h3>
			کامل ترین و کاربر پسندترین سایت فروشگاه
		</h3>
	</div>
</div>
<div class="row text-center">   
    <div class="col-sm-4">
        <img src="/public/img/why-3.png" width="70px" alt="مرحله ۱">
        <div class="half-seperate"></div>
        <h5 class="bold">
        گارانتی زمان تحول محصول
        <div class="seperate"></div>
        <small>تیم ما همواره سفارشات شما را تا لحظه رسیدن محصول به دستتان پیگیری می نمایند.</small>
        </h5>
        <div class="half-seperate"></div>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-2.png" width="70px" alt="مرحله ۲">
        <div class="half-seperate"></div>
        <h5 class="bold">
        راحتی کاربر در استفاده از سایت
        <div class="half-seperate"></div>
        <small>در تمام مراحل سفارش از جمله انتخاب محصول و ثبت نام و آدرس دهی تنها به راحتی شما اندیشیدیم.</small>
        </h5>
    </div>
    <div class="col-sm-4">
        <img src="/public/img/why-1.png" width="60px" alt="مرحله ۳">
        <div class="half-seperate"></div>
        <h5 class="bold">
            مشتری مداری و کاهش هزینه ها
            <div class="half-seperate"></div>
        <small>مبلغ به عنوان مالیات بر ارزش افزوده و هزینه بسته بندی گرفته نمی شود و نیز تخفیف‌هایی داریم.</small>
        </h5>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 text-center">
        @include('common.slider')
    </div>
    <div class="col-sm-6">
        @include('common.slider2')
    </div>
</div>
<div class="row text-center back-nice">
    <div class="col-xs-12">
        <h4>غرفه رستوران</h4>
    </div>
</div>
<div class="seperate"></div>
<div class="reyli-container">
    <div class="flush-box" onclick="goRight()">
        <div class="flush right-side"></div>
    </div>
    <div class="flush-box flush-box-left" onclick="goLeft()">
        <div class="flush"></div>
    </div>
    <div class="reyli-item-box">
        @foreach(\App\Models\Product::where('type_id', 7)->get() as $product)
        <div class="reyli-item text-center">
            <a href="/product/{{ $product->id }}-{{ $product->name }}" style="text-decoration: none;">
                <div class="half-seperate"></div>
                <div class="product-image-container">
                    <img src="/storage/product/{{$product->image->id}}-{{$product->image->name}}" 
                    class="product-image" alt="{{ $product->name }}">
                </div>
                <div class="half-seperate"></div>
                <div class="bold " style="color: #333">
                    <span class="">{{ $product->name }}</span>
                </div>
                <div class="half-seperate"></div>
                <div style="color: #777">
                    <span>{{ $product->bastebandi }}</span>
                    <span>{{ \Nopaad\Persian::correct( $product->toharbastebandi ) }} تایی</span>
                </div>
                <div class="half-seperate"></div>
                <div>
                    <span style="color: #555;font-size: 115%;">
                        {{ \Nopaad\Persian::correct( number_format( $product->price ) , 0, '',',') }} 
                        تومان                       
                    </span>
                </div>  
            </a>
        </div>
        @endforeach
    </div>
</div>
<div class="row text-center back-nice">
    <div class="col-xs-12">
        <h4>غرفه کافی شاپ</h4>
    </div>
</div>
<div class="reyli-container">
    <div class="flush-box" onclick="goRight()">
        <div class="flush right-side"></div>
    </div>
    <div class="flush-box flush-box-left" onclick="goLeft()">
        <div class="flush"></div>
    </div>
    <div class="reyli-item-box">
        @foreach(\App\Models\Product::where('type_id', 8)->get() as $product)
        <div class="reyli-item text-center">
            <a href="/product/{{ $product->id }}-{{ $product->name }}">
                <div class="half-seperate"></div>
                <div class="product-image-container">
                    <img src="/storage/product/{{$product->image->id}}-{{$product->image->name}}" 
                    class="product-image" alt="{{ $product->name }}">
                </div>
                <div class="half-seperate"></div>
                <div class="bold big-size" style="color:#3361ff">
                    <span class="">{{ $product->name }}</span>
                </div>
                <div class="half-seperate"></div>
                <div style="color: #999">
                    <span>{{ $product->bastebandi }}</span>
                    <span>{{ \Nopaad\Persian::correct( $product->toharbastebandi ) }} تایی</span>
                </div>
                <div class="half-seperate"></div>
                <div>
                    <span style="color:rgba(60,155,10,1);margin-left: 10px" class="bold big-size">
                        {{ \Nopaad\Persian::correct( $product->price ) }} 
                        تومان                       
                    </span>
                </div>  
            </a>
        </div>
        @endforeach
    </div>
</div>
@endsection
@push('script')
<script src="/public/js/jquery-lazy.js"></script>
<script type="text/javascript">
$("img.lazy").lazyload({
    // event : "sporty"
    // threshold : 200
    // effect : "fadeIn"
});
</script>
<script>
    var left = 1400;
    function goRight(event) {
        left = left + 200;
        $('.reyli-item-box').stop().animate({
            scrollLeft: left
        }, 'slow','swing',stop(true));
    }
    function goLeft(event) {
        left = left - 200;
        $('.reyli-item-box').stop().animate({
            scrollLeft: left
        }, 'slow','swing',stop(true));
    }

</script>
@endpush

