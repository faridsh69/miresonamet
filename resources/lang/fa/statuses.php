<?php
return [
	'select' => 'انتخاب',
	'address' => 'آدرس دهی',
	'checkout' => 'قبل پرداخت',
	'failed' => 'پرداخت نا موفق',

	'paid' => 'پرداخت شده', // confirm
	'paycredit' => 'پرداخت شده با اعتبار', //confirm
	'willpay' => 'پرداخت در محل', // confirm

	'prepare' => 'آماده سازی لیست', // proccess
	'preparewillpay' => 'آماده سازی لیست شما', // proccess

	'send' => 'سفارش در راه است', // proccess
	'sendwillpay' => 'سفارش در راه است و هزینه در محل پرداخت می شود', // proccess

	'receive' => 'تحویل داده شده',
	'receivewillpay' => 'تحویل داده شده و هزینه دریافت شد',

	'reject' => 'ارسال ناموفق',
	'rejectwillpay' => 'ارسال ناموفق و عدم دریافت هزینه',

	// 'confirm' => 'درانتظار تایید',
	// 'check' => 'در صف بررسی',
	// 'process' => 'در حال پردازش',
];