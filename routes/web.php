<?php

Route::get('masir2', 'MasterController@getMasir2');
Route::get('masir', 'MasterController@getMasir');
Route::get('search/{name}', 'MasterController@getSearch');
Route::get('', 'MasterController@getHome');
Route::get('sabadekharid', 'MasterController@getSabadekharid');
Route::get('type/{id}', 'MasterController@getType');
Route::get('product/{id}', 'MasterController@getProduct');
Route::get('address/{id}', 'MasterController@getAddress'); // it means address dehi
Route::post('address/{id}', 'MasterController@postAddress'); // it means address dehi
Route::post('put-address', 'MasterController@postPutAddress'); // it means address dehi
Route::get('checkout/{id}', 'MasterController@getCheckout');
Route::get('payment/{id}', 'MasterController@getPayment');
Route::get('paymentInLocation/{id}', 'MasterController@getPaymentInLocation');
Route::get('paymentWithCredit/{id}', 'MasterController@getPaymentWithCredit');
Route::get('verify', 'MasterController@getVerify');
Route::get('verifyCredit', 'AdminController@getVerifyCredit');
Route::post('forget-password', 'MasterController@postForgetPassword');
Route::get('language/{id}', 'MasterController@getLanguage')->middleware('can:developer');
Route::get('verificate/{id}', 'MasterController@getVerificateId')->middleware('can:developer');
Route::post('set-comment', 'MasterController@postSetComment')->middleware('auth');
Route::group(['prefix' => 'user'], function () {
	Route::get('login', 'UserController@getLogin');
	Route::post('login', 'UserController@postLogin');
	Route::get('register', 'UserController@getRegister');
	Route::post('register', 'UserController@postRegister');
});
Route::group(['prefix' => 'admin', 'middleware' => ['auth']], function () {

	Route::get('dashboard', 'AdminController@getDashboard');
	Route::get('profile', 'AdminController@getProfile');
	Route::post('profile', 'AdminController@postProfile');
	Route::get('change-password', 'AdminController@getChangePassword');
	Route::post('change-password', 'AdminController@postChangePassword');
	Route::get('charge-credit', 'AdminController@getChargeCredit');
	Route::post('charge-credit', 'AdminController@postChargeCredit');
	// Route::get('address/remove/{id}', 'AdminController@getRemoveAddress');
	Route::get('my-order', 'AdminController@getMyOrder');
	Route::get('my-payment', 'AdminController@getMyPayment');
	Route::get('logout', 'AdminController@getLogout');
	Route::group(['middleware' => ['can:secretary']], function () {
		Route::get('product', 'AdminController@getProduct');
		Route::post('product', 'AdminController@postProduct');
		Route::get('product/edit/{id}', 'AdminController@getEditProduct');
		Route::post('product/edit/{id}', 'AdminController@postEditProduct');
		Route::get('product/remove/{id}', 'AdminController@getRemoveProduct');

		Route::get('type', 'AdminController@getType');
		Route::post('type', 'AdminController@postType');
		Route::get('type/edit/{id}', 'AdminController@getEditType');
		Route::post('type/edit/{id}', 'AdminController@postEditType');
		Route::get('type/remove/{id}', 'AdminController@getRemoveType');

		Route::get('payment/manage', 'AdminController@getPaymentManage');

		Route::get('order/manage', 'AdminController@getOrderManage');
		Route::get('order/manage/edit/{id}', 'AdminController@getOrderManageEdit');
		Route::post('order/manage/edit/{id}', 'AdminController@postOrderManageEdit');

		Route::get('print/{id}', 'AdminController@getPrint');
		Route::get('approve/{id}', 'AdminController@getApprove');
		Route::get('send/{id}', 'AdminController@getSend');
		Route::get('receive/{id}', 'AdminController@getReceive');
		Route::get('reject/{id}', 'AdminController@getReject');
	});
	Route::group(['middleware' => ['can:manager']], function () {
		Route::get('user/manage', 'AdminController@getUserManage');
		Route::get('user/login/{id}', 'AdminController@getUserLogin');
		Route::get('notice/manage', 'AdminController@getNoticeManage');
		Route::post('notice/sms', 'AdminController@postNoticeSms');
		Route::post('notice/email', 'AdminController@postNoticeEmail');
		Route::get('notify/{id}', 'MasterController@getNotifyOrder');
		Route::post('role', 'AdminController@postRole');
		Route::get('comment', 'AdminController@getComment');
		Route::get('shop', 'AdminController@getShop');
		Route::post('shop', 'AdminController@postShop');
		Route::get('report', 'AdminController@getReport');
		Route::post('report/variz', 'AdminController@postReportVariz');
	});
	Route::group(['middleware' => ['can:developer']], function () {
		Route::post('upload', 'AdminController@postUpload');
		Route::get('log', 'AdminController@getLog');
	});	
});
