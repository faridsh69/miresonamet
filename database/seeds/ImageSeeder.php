<?php

use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Image::firstOrCreate(['id' => 1, 'description' => 'logo', 'name' => 'logo.jpg']);
    	for($i=2;$i<19;$i++)
    	{
			\App\Models\Image::firstOrCreate(['id' => $i, 'description' => 'product', 'name' => $i.'.jpg' ]);
    	}
    }
}

