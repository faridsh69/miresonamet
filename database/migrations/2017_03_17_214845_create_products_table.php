<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('price')->unsigned()->nullable();
            $table->integer('off')->unsigned()->default(0);
            $table->integer('type_id')->unsigned()->nullable();
            $table->foreign('type_id')->references('id')->on('types');
            $table->string('bastebandi')->nullable();
            $table->integer('toharbastebandi')->nullable();
            $table->integer('mojodi')->unsigned()->nullable();
            $table->string('description')->nullable();
            $table->string('rang')->nullable();
            $table->string('jens')->nullable();
            $table->string('barcode')->nullable();
            $table->string('sazande')->nullable();
            $table->string('garanti')->nullable();
            $table->string('size')->nullable();
            $table->string('code')->nullable();
            $table->string('vazn')->nullable();
            $table->integer('year')->unsigned()->nullable();
            $table->string('applicant')->nullable();
            $table->boolean('ready')->default(1);
            $table->integer('image_id')->unsigned()->nullable();
            $table->foreign('image_id')->references('id')->on('images');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
